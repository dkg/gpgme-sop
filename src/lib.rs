use std::{
    io::{self, Write},
    path::{Path, PathBuf},
    time::SystemTime,
};

use tempfile::TempDir;

use gpgme::{
    Context,
    EncryptFlags,
    Protocol,
};

use sop::*;

/// For debugging.
const KEEP_HOMEDIRS: bool = false;

pub struct GPGME {
    gpg_path: PathBuf,
}

impl Default for GPGME {
    fn default() -> Self {
        Self::with_gpg_path(
            std::env::var("GNUPG_BIN").unwrap_or("/usr/bin/gpg".into()))
    }
}

impl<'a> GPGME {
    /// Creates a [`sop::SOP`] implementation with an explicit
    /// path to the GnuPG executable.
    ///
    /// To use either the engine specified by the environment variable
    /// `GNUPG_BIN` or `/usr/bin/gpg`, use `GPGME::default()`.
    pub fn with_gpg_path<P: AsRef<Path>>(path: P) -> Self {
        GPGME {
            gpg_path: path.as_ref().into(),
        }
    }
}

impl sop::SOP for GPGME {
    fn version<'b>(&'b self) -> Result<Box<dyn sop::Version + 'b>> {
        Version::new(self)
    }
    fn generate_key<'b>(&'b self) -> Result<Box<dyn sop::GenerateKey + 'b>> {
        GenerateKey::new(self)
    }
    fn extract_cert<'b>(&'b self) -> Result<Box<dyn sop::ExtractCert + 'b>> {
        ExtractCert::new(self)
    }
    fn sign<'b>(&'b self) -> Result<Box<dyn sop::Sign + 'b>> {
        Sign::new(self)
    }
    fn verify<'b>(&'b self) -> Result<Box<dyn sop::Verify + 'b>> {
        Verify::new(self)
    }
    fn encrypt<'b>(&'b self) -> Result<Box<dyn sop::Encrypt + 'b>> {
        Encrypt::new(self)
    }
    fn decrypt<'b>(&'b self) -> Result<Box<dyn sop::Decrypt + 'b>> {
        Decrypt::new(self)
    }
    fn armor<'b>(&'b self) -> Result<Box<dyn sop::Armor + 'b>> {
        Err(Error::NotImplemented)
    }
    fn dearmor<'b>(&'b self) -> Result<Box<dyn sop::Dearmor + 'b>> {
        Err(Error::NotImplemented)
    }
}

struct Version {
    gpg: GnuPG,
}

impl<'a> Version {
    fn new(gpgme: &'a GPGME) -> Result<Box<dyn sop::Version<'a> + 'a>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        Ok(Box::new(Self {
            gpg,
        }))
    }
}

impl<'a> sop::Version<'a> for Version {
    fn frontend(&self) -> Result<sop::VersionInfo> {
        Ok(sop::VersionInfo {
            name: "gpgme-sop".into(),
            version: env!("CARGO_PKG_VERSION").into(),
        })
    }

    fn backend(&self) -> Result<sop::VersionInfo> {
        Ok(sop::VersionInfo {
            name: "GnuPG".into(),
            version: self.gpg.ctx()?.engine_info().version().unwrap().into(),
        })
    }
}

struct GenerateKey<'a> {
    #[allow(dead_code)]
    gpgme: &'a GPGME,
    gpg: GnuPG,
    ctx: Context,
    userids: Vec<String>,
}

impl<'a> GenerateKey<'a> {
    fn new(gpgme: &'a GPGME) -> Result<Box<dyn sop::GenerateKey<'a> + 'a>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            userids: Default::default(),
        }))
    }
}

impl<'a> sop::GenerateKey<'a> for GenerateKey<'a> {
    fn no_armor(mut self: Box<Self>) -> Box<dyn sop::GenerateKey<'a> + 'a> {
        self.ctx.set_armor(false);
        self
    }

    fn userid(mut self: Box<Self>, userid: &str)
              -> Box<dyn sop::GenerateKey<'a> + 'a> {
        self.userids.push(userid.into());
        self
    }

    fn generate(mut self: Box<Self>) -> Result<Box<dyn sop::Ready + 'a>> {
        if self.userids.len() == 0 {
            return Err(Error::MissingArg);
        }

        let r = self.ctx.create_key_with_flags(
            &self.userids[0],
            "default",
            None,
            gpgme::CreateKeyFlags::NOPASSWD).map_err(sop_error)?;
        let fp = r.fingerprint().unwrap();
        let key = self.ctx.get_key(fp).map_err(sop_error)?;
        for u in &self.userids[1..] {
            self.ctx.add_uid(&key, u).map_err(sop_error)?;
        }

        Ok(Box::new(KeyExportReady {
            gpgme: self.gpgme,
            gpg: self.gpg,
            ctx: self.ctx,
            keys: vec![key],
            mode: gpgme::ExportMode::SECRET,
        }))
    }
}

struct KeyExportReady<'a> {
    #[allow(dead_code)]
    gpgme: &'a GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    keys: Vec<gpgme::keys::Key>,
    mode: gpgme::ExportMode,
}

impl sop::Ready for KeyExportReady<'_> {
    fn write_to(mut self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<()>
    {
        // XXX: Currently, gpgme::IntoData is not implemented for &mut
        // dyn io::Write.  Therefore, we cannot stream.
        let mut xxx = Vec::new();
        self.ctx.export_keys(&self.keys, self.mode, &mut xxx)
            .map_err(sop_error)?;
        sink.write_all(&xxx)?;
        Ok(())
    }
}

struct ExtractCert<'a> {
    #[allow(dead_code)]
    gpgme: &'a GPGME,
    gpg: GnuPG,
    ctx: Context,
}

impl<'a> ExtractCert<'a> {
    fn new(gpgme: &'a GPGME) -> Result<Box<dyn sop::ExtractCert<'a> + 'a>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
        }))
    }
}

impl<'a> sop::ExtractCert<'a> for ExtractCert<'a> {
    /// Disables armor encoding.
    fn no_armor(mut self: Box<Self>) -> Box<dyn sop::ExtractCert<'a> + 'a> {
        self.ctx.set_armor(false);
        self
    }

    /// Extracts the cert from `key`.
    fn key(self: Box<Self>, key: &mut (dyn io::Read + Send + Sync))
           -> Result<Box<dyn Ready + 'a>> {
        self.keys(key)
    }

    /// Extracts the certs from `keys`.
    fn keys(mut self: Box<Self>, keys: &mut (dyn io::Read + Send + Sync))
            -> Result<Box<dyn Ready + 'a>> {
        // XXX: Currently, gpgme::IntoData is not implemented for &mut
        // dyn io::Read.  Therefore, we cannot stream.
        let mut xxx = Vec::new();
        keys.read_to_end(&mut xxx)?;
        self.ctx.import(xxx).map_err(sop_error)?;
        let keys = self.ctx.keys().map_err(sop_error)?
            .map(|rk| rk.map_err(sop_error))
            .collect::<Result<Vec<_>>>()?;

        Ok(Box::new(KeyExportReady {
            gpgme: self.gpgme,
            gpg: self.gpg,
            ctx: self.ctx,
            keys,
            mode: gpgme::ExportMode::empty(),
        }))
    }
}

struct Sign<'a> {
    #[allow(dead_code)]
    gpgme: &'a GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
}

impl<'a> Sign<'a> {
    fn new(gpgme: &'a GPGME) -> Result<Box<dyn sop::Sign<'a> + 'a>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
        }))
    }
}

impl<'a> sop::Sign<'a> for Sign<'a> {
    fn no_armor(mut self: Box<Self>) -> Box<dyn sop::Sign<'a> + 'a> {
        self.ctx.set_armor(false);
        self
    }

    fn mode(mut self: Box<Self>, mode: SignAs) -> Box<dyn sop::Sign<'a> + 'a> {
        if let SignAs::Text = mode {
            self.ctx.set_text_mode(true);
        }
        self
    }

    fn key(self: Box<Self>, key: &mut (dyn io::Read + Send + Sync))
           -> Result<Box<dyn sop::Sign<'a> + 'a>> {
        self.keys(key)
    }

    fn keys(mut self: Box<Self>, keys: &mut (dyn io::Read + Send + Sync))
            -> Result<Box<dyn sop::Sign<'a> + 'a>> {
        let mut xxx = Vec::new();
        keys.read_to_end(&mut xxx)?;

        for import in self.ctx.import(xxx).map_err(sop_error)?.imports() {
            // Handle import errors.
            import.result().map_err(sop_error)?;

            let key = self.ctx.get_key(dbg!(import.fingerprint().unwrap()))
                .expect("just imported");
            self.ctx.add_signer(&key).map_err(sop_error)?;
        }

        Ok(self)
    }

    fn data(self: Box<Self>, data: &'a mut (dyn io::Read + Send + Sync))
            -> Result<Box<dyn ReadyWithResult<sop::Micalg> + 'a>> {
        Ok(Box::new(SignReady {
            sign: *self,
            data,
        }))
    }
}

struct SignReady<'a> {
    sign: Sign<'a>,
    data: &'a mut (dyn io::Read + Send + Sync),
}

impl<'a> sop::ReadyWithResult<sop::Micalg> for SignReady<'a> {
    fn write_to(mut self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<sop::Micalg>
    {
        let mut xxx = Vec::new();
        self.data.read_to_end(&mut xxx)?;
        let mut sig = Vec::new();
        let result =
            self.sign.ctx.sign(gpgme::SignMode::Detached, &xxx, &mut sig)
            .map_err(sop_error)?;
        sink.write_all(&sig)?;
        Ok(result.new_signatures().next()
           .map(|sig| (sig.hash_algorithm().raw() as u8).into())
           .unwrap_or(sop::Micalg::Unknown("unknown".into())))
    }
}

struct Verify<'a> {
    #[allow(dead_code)]
    gpgme: &'a GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    not_before: Option<SystemTime>,
    not_after: Option<SystemTime>,
}

impl<'a> Verify<'a> {
    fn new(gpgme: &'a GPGME) -> Result<Box<dyn sop::Verify<'a> + 'a>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            not_before: None,
            not_after: None,
        }))
    }
}

impl<'a> sop::Verify<'a> for Verify<'a> {
    fn not_before(mut self: Box<Self>, t: SystemTime)
                  -> Box<dyn sop::Verify<'a> + 'a> {
        self.not_before = Some(t);
        self
    }

    fn not_after(mut self: Box<Self>, t: SystemTime)
                 -> Box<dyn sop::Verify<'a> + 'a> {
        self.not_after = Some(t);
        self
    }

    fn cert(self: Box<Self>, cert: &mut (dyn io::Read + Send + Sync))
            -> Result<Box<dyn sop::Verify<'a> + 'a>> {
        self.certs(cert)
    }

    fn certs(mut self: Box<Self>, certs: &mut (dyn io::Read + Send + Sync))
            -> Result<Box<dyn sop::Verify<'a> + 'a>> {
        let mut xxx = Vec::new();
        certs.read_to_end(&mut xxx)?;

        for import in self.ctx.import(xxx).map_err(sop_error)?.imports() {
            // Handle import errors.
            import.result().map_err(sop_error)?;
        }
        Ok(self)
    }

    fn signatures(self: Box<Self>,
                  signatures: &'a mut (dyn io::Read + Send + Sync))
                  -> Result<Box<dyn sop::VerifySignatures + 'a>> {
        Ok(Box::new(VerifySignatures {
            verify: *self,
            signatures: signatures,
        }))
    }
}

struct VerifySignatures<'a> {
    verify: Verify<'a>,
    signatures: &'a mut (dyn io::Read + Send + Sync),
}

impl sop::VerifySignatures for VerifySignatures<'_> {
    fn data(mut self: Box<Self>, data: &mut (dyn io::Read + Send + Sync))
            -> Result<Vec<sop::Verification>> {
        let mut xxx = Vec::new();
        data.read_to_end(&mut xxx)?;
        let mut signatures = Vec::new();
        self.signatures.read_to_end(&mut signatures)?;

        let sigs = self.verify.ctx.verify_detached(signatures, xxx)
            .map_err(sop_error)?;
        let verifications =
            sigs2verifications(sigs.signatures(),
                               self.verify.not_before, self.verify.not_after)?;

        if verifications.is_empty() {
            Err(Error::NoSignature)
        } else {
            Ok(verifications)
        }
    }
}

/// Converts `gpgme::Signature`s to `sop::Verification`s.
fn sigs2verifications<'a>(sigs: impl IntoIterator<Item = gpgme::Signature<'a>>,
                          not_before: Option<SystemTime>,
                          not_after: Option<SystemTime>)
                          -> sop::Result<Vec<sop::Verification>> {
    let mut verifications = Vec::new();
    for sig in sigs {
        if sig.status().is_err()
        // XXX: Status is never GOOD or GREEN for me, despite
        // always-trust, so checking for RED instead.
            || sig.summary().contains(
                gpgme::SignatureSummary::RED)
        {
            continue;
        }

        let creation_time =
            if let Some(t) = sig.creation_time() {
                t
            } else {
                continue;
            };

        if let Some(not_before) = not_before {
            if creation_time < not_before {
                eprintln!(
                    "Signature by {} was created before \
                     the --not-before date.",
                    sig.fingerprint().unwrap());
                continue;
            }
        }

        if let Some(not_after) = not_after {
            if creation_time > not_after {
                eprintln!(
                    "Signature by {} was created after \
                     the --not-after date.",
                    sig.fingerprint().unwrap());
                continue;
            }
        }

        verifications.push(Verification::new(
            creation_time,
            sig.fingerprint().unwrap(),
            // XXX: Is this right?  Is sig.key() None if
            // the primary key is the signer?  Or is
            // something else wrong here?
            sig.key()
                .map(|key| key.fingerprint().unwrap().to_string())
                .unwrap_or(sig.fingerprint().unwrap().to_string()),
            None)?);
    }
    Ok(verifications)
}

struct Encrypt<'a> {
    #[allow(dead_code)]
    gpgme: &'a GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    passwords: Vec<Password>,
    recipients: Vec<gpgme::keys::Key>,
    do_sign: bool,
}

impl<'a> Encrypt<'a> {
    fn new(gpgme: &'a GPGME) -> Result<Box<dyn sop::Encrypt<'a> + 'a>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            passwords: Vec::new(),
            recipients: Vec::new(),
            do_sign: false,
        }))
    }
}

impl<'a> sop::Encrypt<'a> for Encrypt<'a> {
    /// Disables armor encoding.
    fn no_armor(mut self: Box<Self>) -> Box<dyn sop::Encrypt<'a> + 'a> {
        self.ctx.set_armor(false);
        self
    }

    /// Sets encryption mode.
    fn mode(mut self: Box<Self>, mode: EncryptAs) -> Box<dyn sop::Encrypt<'a> + 'a> {
        if let EncryptAs::Text = mode {
            self.ctx.set_text_mode(true);
            // XXX: Can we control the literal packets data type
            // field?
        }
        self
    }

    fn sign_with_key(self: Box<Self>,
                     key: &mut (dyn io::Read + Send + Sync))
                     -> Result<Box<dyn sop::Encrypt<'a> + 'a>> {
        self.sign_with_keys(key)
    }

    fn sign_with_keys(mut self: Box<Self>,
                      keys: &mut (dyn io::Read + Send + Sync))
                      -> Result<Box<dyn sop::Encrypt<'a> + 'a>> {
        let mut xxx = Vec::new();
        keys.read_to_end(&mut xxx)?;

        for import in self.ctx.import(xxx).map_err(sop_error)?.imports() {
            // Handle import errors.
            import.result().map_err(sop_error)?;

            let key = self.ctx.get_key(import.fingerprint().unwrap())
                .expect("just imported");
            self.ctx.add_signer(&key).map_err(sop_error)?;
            self.do_sign = true;
        }

        Ok(self)
    }

    /// Encrypts with the given password.
    fn with_password(mut self: Box<Self>, password: Password)
                     -> Result<Box<dyn sop::Encrypt<'a> + 'a>> {
        if ! self.passwords.is_empty() {
            // Encrypting with more than one password is not
            // supported.
            return Err(Error::NotImplemented);
        }

        self.passwords.push(password);
        Ok(self)
    }

    fn with_cert(self: Box<Self>, cert: &mut (dyn io::Read + Send + Sync))
                 -> Result<Box<dyn sop::Encrypt<'a> + 'a>> {
        self.with_certs(cert)
    }

    fn with_certs(mut self: Box<Self>, certs: &mut (dyn io::Read + Send + Sync))
                  -> Result<Box<dyn sop::Encrypt<'a> + 'a>> {
        let mut xxx = Vec::new();
        certs.read_to_end(&mut xxx)?;

        for import in self.ctx.import(xxx).map_err(sop_error)?.imports() {
            // Handle import errors.
            import.result().map_err(sop_error)?;

            let key = self.ctx.get_key(import.fingerprint().unwrap())
                .expect("just imported");
            self.recipients.push(key);
        }
        Ok(self)
    }

    /// Encrypts the given data yielding the ciphertext.
    fn plaintext(self: Box<Self>,
                 plaintext: &'a mut (dyn io::Read + Send + Sync))
                 -> Result<Box<dyn Ready + 'a>> {
        Ok(Box::new(EncryptReady {
            encrypt: *self,
            plaintext,
        }))
    }
}

struct EncryptReady<'a> {
    encrypt: Encrypt<'a>,
    plaintext: &'a mut (dyn io::Read + Send + Sync),
}

impl<'a> sop::Ready for EncryptReady<'a> {
    fn write_to(mut self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<()>
    {
        let mut plaintext = Vec::new();
        self.plaintext.read_to_end(&mut plaintext)?;

        let mut flags = EncryptFlags::ALWAYS_TRUST;
        if ! self.encrypt.passwords.is_empty() {
            flags |= EncryptFlags::SYMMETRIC;
        }

        let password = if self.encrypt.passwords.is_empty() {
            None
        } else {
            Some(self.encrypt.passwords.swap_remove(0))
        };
        let mut ctx =
            if let Some(p) = password.as_ref()
        {
            self.encrypt.ctx
                .set_passphrase_provider(move |_: gpgme::PassphraseRequest,
                                         w: &mut dyn Write| {
                                             Ok(w.write_all(p.as_ref()).unwrap())
                                         })
        } else {
            self.encrypt.ctx.into()
        };

        let mut ciphertext = Vec::new();
        if self.encrypt.recipients.is_empty() && password.is_none() {
            return Err(Error::MissingArg);
        } else if self.encrypt.recipients.is_empty() {
            if self.encrypt.do_sign {
                return Err(Error::NotImplemented);
            }
            ctx.encrypt_symmetric(plaintext, &mut ciphertext)
                .map_err(sop_error)?;
        } else if ! self.encrypt.do_sign {
            ctx.encrypt_with_flags(
                self.encrypt.recipients.iter(),
                plaintext,
                &mut ciphertext,
                flags)
                .map_err(sop_error)?;
        } else {
            ctx.sign_and_encrypt_with_flags(
                self.encrypt.recipients.iter(),
                plaintext,
                &mut ciphertext,
                flags)
                .map_err(sop_error)?;
        }
        sink.write_all(&ciphertext)?;

        Ok(())
    }
}

struct Decrypt<'a> {
    #[allow(dead_code)]
    gpgme: &'a GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    not_before: Option<SystemTime>,
    not_after: Option<SystemTime>,
    passwords: Vec<Password>,
}

impl<'a> Decrypt<'a> {
    fn new(gpgme: &'a GPGME) -> Result<Box<dyn sop::Decrypt<'a> + 'a>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            not_before: None,
            not_after: None,
            passwords: Vec::new(),
        }))
    }
}

impl<'a> sop::Decrypt<'a> for Decrypt<'a> {
    /// Makes SOP consider signatures before this date invalid.
    fn verify_not_before(mut self: Box<Self>, t: SystemTime)
                         -> Box<dyn sop::Decrypt<'a> + 'a> {
        self.not_before = Some(t);
        self
    }

    /// Makes SOP consider signatures after this date invalid.
    fn verify_not_after(mut self: Box<Self>, t: SystemTime)
                        -> Box<dyn sop::Decrypt<'a> + 'a> {
        self.not_after = Some(t);
        self
    }

    fn verify_with_cert(self: Box<Self>,
                        cert: &mut (dyn io::Read + Send + Sync))
                        -> Result<Box<dyn sop::Decrypt<'a> + 'a>> {
        self.verify_with_certs(cert)
    }

    fn verify_with_certs(mut self: Box<Self>,
                         certs: &mut (dyn io::Read + Send + Sync))
                         -> Result<Box<dyn sop::Decrypt<'a> + 'a>> {
        let mut xxx = Vec::new();
        certs.read_to_end(&mut xxx)?;

        for import in self.ctx.import(xxx).map_err(sop_error)?.imports() {
            // Handle import errors.
            import.result().map_err(sop_error)?;
        }

        Ok(self)
    }

    /// Tries to decrypt with the given session key.
    fn with_session_key(self: Box<Self>, _sk: sop::SessionKey)
                        -> Result<Box<dyn sop::Decrypt<'a> + 'a>> {
        Err(Error::NotImplemented)
    }

    /// Tries to decrypt with the given password.
    fn with_password(mut self: Box<Self>, password: Password)
                     -> Result<Box<dyn sop::Decrypt<'a> + 'a>> {
        self.passwords.push(password);
        Ok(self)
    }

    fn with_key(self: Box<Self>, key: &mut (dyn io::Read + Send + Sync))
                -> Result<Box<dyn sop::Decrypt<'a> + 'a>> {
        self.with_keys(key)
    }

    fn with_keys(mut self: Box<Self>, keys: &mut (dyn io::Read + Send + Sync))
                 -> Result<Box<dyn sop::Decrypt<'a> + 'a>> {
        let mut xxx = Vec::new();
        keys.read_to_end(&mut xxx)?;

        for import in self.ctx.import(xxx).map_err(sop_error)?.imports() {
            // Handle import errors.
            import.result().map_err(sop_error)?;
        }
        Ok(self)
    }

    /// Decrypts `ciphertext`, returning verification results and
    /// plaintext.
    fn ciphertext(self: Box<Self>,
                  ciphertext: &'a mut (dyn io::Read + Send + Sync))
                  -> Result<Box<dyn sop::ReadyWithResult<(Option<sop::SessionKey>,
                                                          Vec<sop::Verification>)> + 'a>>
    {
        Ok(Box::new(DecryptReady {
            decrypt: *self,
            ciphertext,
        }))
    }
}

struct DecryptReady<'a> {
    decrypt: Decrypt<'a>,
    ciphertext: &'a mut (dyn io::Read + Send + Sync),
}

impl<'a> sop::ReadyWithResult<(Option<sop::SessionKey>, Vec<sop::Verification>)>
    for DecryptReady<'a>
{
    fn write_to(self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<(Option<sop::SessionKey>, Vec<sop::Verification>)> {
        let mut passwords = self.decrypt.passwords;
        let mut ctx =
            self.decrypt.ctx.set_passphrase_provider(
                move |_: gpgme::PassphraseRequest, w: &mut dyn Write| {
                    // XXX: GPGME only polls once for the
                    // password, not sure why or what to do about
                    // it.
                    if let Some(p) = passwords.pop() {
                        w.write_all(p.as_ref()).unwrap()
                    }
                    Ok(())
                });

        let mut ciphertext = Vec::new();
        self.ciphertext.read_to_end(&mut ciphertext)?;

        let mut plaintext = Vec::new();
        let (_, sigs) = ctx.decrypt_and_verify(ciphertext,
                                               &mut plaintext)
            .map_err(sop_error)?;
        let verifications =
            sigs2verifications(sigs.signatures(),
                               self.decrypt.not_before,
                               self.decrypt.not_after)?;

        sink.write_all(&plaintext)?;
        Ok((None, verifications))
    }
}

/// Maps Sequoia errors to sop::Errors.
///
/// XXX: This is a bit of a friction point.  We likely need to improve
/// the SOP spec a little.
fn sop_error(e: gpgme::Error) -> sop::Error {
    // XXX

    eprintln!("Warning: Untranslated error: {}", e);
    sop::Error::BadData
}

struct GnuPG {
    engine: PathBuf,
    homedir: TempDir,
}

impl GnuPG {
    fn new<P: AsRef<Path>>(engine: P) -> Result<GnuPG> {
        let homedir = TempDir::new()?;
        std::fs::write(homedir.path().join("gpg.conf"),
                       "batch\nalways-trust\n")?;
        Ok(GnuPG {
            engine: engine.as_ref().into(),
            homedir,
        })
    }

    fn ctx(&self) -> Result<Context> {
        let mut ctx = Context::from_protocol(Protocol::OpenPgp)
            .map_err(sop_error)?;
        ctx.set_pinentry_mode(gpgme::PinentryMode::Loopback)
            .map_err(sop_error)?;
        ctx.set_armor(true);
        ctx.set_engine_path(
            String::from(self.engine.to_str().unwrap()))
            .map_err(sop_error)?;
        ctx.set_engine_home_dir(
            String::from(self.homedir.path().to_string_lossy()))
            .map_err(sop_error)?;
        Ok(ctx)
    }
}

impl Drop for GnuPG {
    fn drop(&mut self) {
        if KEEP_HOMEDIRS {
            let homedir =
                std::mem::replace(&mut self.homedir, TempDir::new().unwrap());
            eprintln!("Leaving GnuPG homedir {:?} for inspection",
                      homedir.into_path());
        }
    }
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;
    use super::*;

    /// This is the example from the SOP spec:
    ///
    ///     sop generate-key "Alice Lovelace <alice@openpgp.example>" > alice.sec
    ///     sop extract-cert < alice.sec > alice.pgp
    ///
    ///     sop sign --as=text alice.sec < statement.txt > statement.txt.asc
    ///     sop verify announcement.txt.asc alice.pgp < announcement.txt
    ///
    ///     sop encrypt --sign-with=alice.sec --as=mime bob.pgp < msg.eml > encrypted.asc
    ///     sop decrypt alice.sec < ciphertext.asc > cleartext.out
    #[test]
    fn sop_examples() -> Result<()> {
        let sop = GPGME::default();

        let alice_sec = sop.generate_key()?
            .userid("Alice Lovelace <alice@openpgp.example>")
            .generate()?
            .to_vec()?;
        let alice_pgp = sop.extract_cert()?
            .key(&mut Cursor::new(&alice_sec))?.to_vec()?;

        let bob_sec = sop.generate_key()?
            .userid("Bob Babbage <bob@openpgp.example>")
            .generate()?
            .to_vec()?;
        let bob_pgp = sop.extract_cert()?
            .key(&mut Cursor::new(&bob_sec))?.to_vec()?;

        let statement = b"Hello World :)";
        let mut data = Cursor::new(&statement);
        let (_micalg, statement_asc) = sop.sign()?
            .mode(SignAs::Text)
            .key(&mut Cursor::new(&alice_sec))?
            .data(&mut data)?
            .to_vec()?;

        let mut statement_asc_cur = Cursor::new(&statement_asc);
        let verifications = sop.verify()?
            .cert(&mut Cursor::new(&alice_pgp))?
            .signatures(&mut statement_asc_cur)?
            .data(&mut Cursor::new(&statement))?;
        assert_eq!(verifications.len(), 1);

        let mut statement_cur = Cursor::new(&statement);
        let ciphertext = sop.encrypt()?
            .sign_with_key(&mut Cursor::new(&alice_sec))?
            .mode(EncryptAs::MIME)
            .with_cert(&mut Cursor::new(&bob_pgp))?
            .plaintext(&mut statement_cur)?
            .to_vec()?;

        let mut ciphertext_cur = Cursor::new(&ciphertext);
        let (_, plaintext) = sop.decrypt()?
            .with_key(&mut Cursor::new(&bob_sec))?
            .ciphertext(&mut ciphertext_cur)?
            .to_vec()?;
        assert_eq!(&plaintext, statement);

        Ok(())
    }
}
